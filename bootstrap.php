<?php

declare(strict_types=1);

/**
 * This is required due to the behavior of
 * \\Fence\\Configuration 'get' method that would
 * make Slim raise an exception when a variable is
 * not defined in the `configuration.json` file.
 */

error_reporting(E_ALL & ~E_USER_WARNING);

require_once __DIR__ . "/vendor/autoload.php";

$path = __DIR__;

loadDotenvFile($path, ".env");
loadDotenvFile($path, ".env.local");

function loadDotenvFile($path, $filename)
{
    if (file_exists("{$path}/{$filename}")) {
        $dotenv = Dotenv\Dotenv::createImmutable($path, $filename);
        $dotenv->load();
    }
}
