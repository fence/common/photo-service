<?php

declare(strict_types=1);

namespace Glance\PhotoService\Tests\Integration;

use Doctrine\DBAL\Driver\Connection;
use Glance\PhotoService\Authorization\Exception\UserHasNotGivenConsent;
use Glance\PhotoService\Photo\Domain\Photo;
use Glance\PhotoService\Photo\Infrastructure\Provider\PhotoProvider;
use Glance\PhotoService\Photo\Infrastructure\Provider\PhotoProviderFactory;
use Glance\PhotoService\UserConsent\Infrastructure\Provider\UserConsentProviderFactory;
use Glance\PhotoService\UserConsent\Infrastructure\Provider\UserConsentProvider;

class PhotoServiceIntegrationTest extends IntegrationBaseTest
{
    /** @var UserConsentProvider */
    private $userConsentProvider;
    /** @var PhotoProvider */
    private $photoProvider;

    const PERSON_ID = 1;
    const APPLICATION_ID = "glance-photo-api-test";
    const CONSENT = false;
    const AGENT_ID = 1;

    public function setUp(): void
    {
        $this->userConsentProvider = UserConsentProviderFactory::getUserConsentInstance(
            getenv("DB_USERNAME"),
            getenv("DB_PASSWORD"),
            getenv("DB_DNS"),
        );

        $this->photoProvider = PhotoProviderFactory::getPhotoProvider(
            getenv("SERVICE_ACCOUNT_USERNAME"),
            getenv("SERVICE_ACCOUNT_PASSWORD"),
            getenv("EXPERIMENT_ENDPOINT"),
            getenv("DB_USERNAME"),
            getenv("DB_PASSWORD"),
            getenv("DB_DNS")
        );
    }

    /** @test */
    public function testGetUserConsentDetailsBeforeConsentInserted(): void
    {
        $userConsent = $this->userConsentProvider->findUserConsentDetailsByPersonId(
            self::PERSON_ID,
            self::APPLICATION_ID
        );
        $this->assertEquals(null, $userConsent);
    }

    /**
     * @test
     * @depends testGetUserConsentDetailsBeforeConsentInserted
     */
    public function testInsertUserConsent(): void
    {
        $userConsent = $this->userConsentProvider->updateUserConsent(
            self::PERSON_ID,
            false,
            self::APPLICATION_ID,
            self::AGENT_ID
        );
        $this->assertEquals(false, $userConsent->hasConsent());
    }

    /**
     * @test
     * @depends testInsertUserConsent
     */
    public function testGetPhotoWithoutConsent(): void
    {
        $this->expectException(UserHasNotGivenConsent::class);
        $this->photoProvider->getPhoto(
            self::PERSON_ID,
            self::APPLICATION_ID
        );
    }

    /**
     * @test
     * @depends testGetPhotoWithoutConsent
     */
    public function testUpdateUserConsent(): void
    {
        $userConsent = $this->userConsentProvider->updateUserConsent(
            self::PERSON_ID,
            true,
            self::APPLICATION_ID,
            self::AGENT_ID
        );
        $this->assertEquals(true, $userConsent->hasConsent());
    }

    /**
     * @test
     * @depends testUpdateUserConsent
     */
    public function testGetUserConsentDetails(): void
    {
        $userConsent = $this->userConsentProvider->findUserConsentDetailsByPersonId(
            self::PERSON_ID,
            self::APPLICATION_ID
        );
        $this->assertEquals(true, $userConsent->hasConsent());
    }

    /**
     * @test
     * @depends testGetUserConsentDetails
     */
    public function testGetPhotoWithConsent(): void
    {
        $photo = $this->photoProvider->getPhoto(
            self::PERSON_ID,
            self::APPLICATION_ID
        );
        $this->assertEquals(Photo::class, get_class($photo));
    }

    /**
     * @test
     * @depends testGetUserConsentDetails
     */
    public function testGetUserConsentFromApplication(): void
    {
        $consentFromApplication = $this->userConsentProvider->findUserConsentDetailsByApplicationId(
            self::APPLICATION_ID
        );
        $this->assertNotNull($consentFromApplication);
    }

    /**
     * @test
     * @depends testGetUserConsentFromApplication
     * */
    public function testGetUserConsentHistoryByPersonId(): void
    {
        $userConsentHistory = $this->userConsentProvider->findUserConsentHistoryDetailsByPersonId(
            self::PERSON_ID,
            self::APPLICATION_ID
        );

        $this->assertNotNull($userConsentHistory);
    }

    /**
     * @test
     * @depends testGetUserConsentFromApplication
     * */
    public function testGetUserConsentHistoryByApplicationId(): void
    {
        $userConsentHistory = $this->userConsentProvider->findUserConsentHistoryDetailsByApplicationId(
            self::APPLICATION_ID
        );

        $this->assertNotNull($userConsentHistory);
    }

    public static function tearDownAfterClass(): void
    {
        $connection = self::$container->get(Connection::class);
        $query = "BEGIN
            DELETE FROM ALLOW_PHOTO WHERE APPLICATION_ID = :varApplicationId;
            DELETE FROM GLANCE_PHOTO_HIST.ALLOW_PHOTO WHERE APPLICATION_ID = :varApplicationId;
        END;
        ";
        $statement = $connection->prepare($query);
        $statement->execute(["varApplicationId" => self::APPLICATION_ID]);
    }
}
