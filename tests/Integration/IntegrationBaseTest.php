<?php

declare(strict_types=1);

namespace Glance\PhotoService\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Driver\OCI8\OCI8Connection;
use DI\ContainerBuilder;

abstract class IntegrationBaseTest extends TestCase
{
    public static $container;

    public static function setUpBeforeClass(): void
    {
        $containerBuilder = new ContainerBuilder();
        $definitions = [
            Connection::class => function () {
                $connection = new OCI8Connection(
                    getenv("DB_USERNAME"),
                    getenv("DB_PASSWORD"),
                    getenv("DB_DNS")
                );
                return $connection;
            }
        ];

        $containerBuilder->addDefinitions($definitions);
        self::$container = $containerBuilder->build();
    }

    public static function getRandomString(
        int $length = 64,
        string $keyspace = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    ): string {
        if ($length < 1) {
            throw new \RangeException("Length must be a positive integer");
        }
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces[] = $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }
}
