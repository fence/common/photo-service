<?php

declare(strict_types=1);

namespace Glance\PhotoService\Tests\Unit\Domain;

use Glance\PhotoService\Shared\Domain\Consent;
use PHPUnit\Framework\TestCase;
use Glance\PhotoService\UserConsent\Domain\AgentId;
use Glance\PhotoService\UserConsent\Domain\UserConsent;
use Glance\PhotoService\Shared\Domain\PersonId;

class UserConsentTest extends TestCase
{
    const PERSON_ID = 1;
    const APPLICATION_ID = "glance-photo-api-test";
    const CONSENT = false;
    const AGENT_ID = 1;

    /** @test */
    public function itShouldCreateAnUserConsent()
    {
        $userConsent = UserConsent::create(
            PersonId::fromInteger(self::PERSON_ID),
            Consent::fromBoolean(self::CONSENT),
            self::APPLICATION_ID,
            AgentId::fromInteger(self::AGENT_ID)
        );

        // Assert
        $this->assertEquals($userConsent->personId()->toInteger(), self::PERSON_ID);
        $this->assertEquals($userConsent->consent()->toBoolean(), self::CONSENT);
        $this->assertEquals($userConsent->agentId()->toInteger(), self::AGENT_ID);
    }

    /** @test */
    public function itShouldUpdateAnUserConsent()
    {
        // Create model
        $userConsent = UserConsent::create(
            PersonId::fromInteger(self::PERSON_ID),
            Consent::fromBoolean(self::CONSENT),
            self::APPLICATION_ID,
            AgentId::fromInteger(self::AGENT_ID)
        );

        $newConsent = Consent::fromBoolean(false);

        $userConsent->updateConsent(
            $newConsent,
            AgentId::fromInteger(self::AGENT_ID)
        );

        // Assert
        $this->assertEquals($userConsent->consent()->toBoolean(), $newConsent->toBoolean());
    }
}
