current-dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# Variables you can set from the command line.
TESTSDIR =
ARGS =

.PHONY: help
help:
	@echo ""
	@echo " Usage:"
	@echo "    make [tasks][options]"
	@echo ""
	@echo " Configuration options:"
	@echo "    ARGS=<extra-arguments>  Append arguments to tasks"
	@echo "    TESTSDIR=<directory>    Specify a different directory to collect testing files"
	@echo ""
	@echo " Available tasks:"
	@echo "    tests                   Run all tests"
	@echo "    unit-tests              Run only unit tests"
	@echo "    integration-tests       Run only integration tests"
	@echo ""


.PHONY: tests
tests:
	@TEST=true ./vendor/bin/phpunit $(TESTSDIR) $(ARGS)

.PHONY: unit-tests
unit-tests:
	@TEST=true ./vendor/bin/phpunit $(TESTSDIR) --testsuite unit $(ARGS)

.PHONY: integration-tests
integration-tests:
	@TEST=true ./vendor/bin/phpunit $(TESTSDIR) --testsuite integration $(ARGS)