<?php

declare(strict_types=1);

namespace Glance\PhotoService\Shared\Domain;

use Glance\PhotoService\Shared\Domain\IntegerId;

final class PersonId extends IntegerId
{
}
