<?php

declare(strict_types=1);

namespace Glance\PhotoService\Shared\Domain;

use InvalidArgumentException;
use Webmozart\Assert\Assert;

class IntegerId
{
    protected $id;

    public function __construct(int $id)
    {
        Assert::greaterThan($id, 0);

        $this->id = $id;
    }

    public static function fromString(string $id)
    {
        return new static((int) $id);
    }

    public static function fromInteger(int $id): self
    {
        return new static($id);
    }

    public static function validated($context = "integer identity", ...$ids): array
    {
        return array_map(
            function ($id) use ($context) {
                return self::assertIsValidId($id, $context);
            },
            $ids
        );
    }

    public static function createNewOrNull(?int $id): ?self
    {
        if (is_null($id)) {
            return null;
        }

        return new static($id);
    }

    public function equals(self $otherId): bool
    {
        return $this->id === $otherId->toInteger();
    }

    public function notEqualsTo(self $otherId): bool
    {
        return !$this->equals($otherId);
    }

    public function toInteger(): int
    {
        return $this->id;
    }

    public function toString(): string
    {
        return (string) $this->id;
    }

    public function __toString()
    {
        return $this->toString();
    }

    private static function assertIsValidId($id, $context = "integer identity"): int
    {
        try {
            $instance = new self((int) $id);

            return $instance->toInteger();
        } catch (InvalidArgumentException $e) {
            throw new InvalidArgumentException("{$context}: {$e->getMessage()}", $e->getCode(), $e);
        }
    }
}
