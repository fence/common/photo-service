<?php

declare(strict_types=1);

namespace Glance\PhotoService\Shared\Domain;

use InvalidArgumentException;

final class Url
{
    private $url;

    public function __construct(string $url)
    {
        $url = trim($url);
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new InvalidArgumentException("Invalid url: $url");
        }

        if (!$this->includesProtocol($url)) {
            $url = 'https://' . $url;
        }

        $this->url = $url;
    }

    public static function fromString(string $url): self
    {
        return new self($url);
    }

    public function toString(): string
    {
        return $this->url;
    }

    private function includesProtocol(string $url): bool
    {
        return (strpos($url, 'http://') !== false)
        || (strpos($url, 'https://') !== false);
    }
}
