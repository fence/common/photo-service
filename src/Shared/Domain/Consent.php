<?php

declare(strict_types=1);

namespace Glance\PhotoService\Shared\Domain;

use InvalidArgumentException;

class Consent
{
    private $consent;
    const MAP = [
        "YES" => "Y",
        "Y" => "Y",
        "NO" => "N",
        "N" => "N"
    ];
    const YES = "Y";
    const NO = "N";

    private function __construct(string $consent)
    {
        $parsedConsent = trim(strtoupper($consent));

        if (!array_key_exists($parsedConsent, self::MAP)) {
            throw new InvalidArgumentException("Invalid consent value: {$consent}");
        }

        $this->consent = self::MAP[$parsedConsent];
    }

    public static function fromString(string $consent)
    {
        return new static($consent);
    }

    public static function fromBoolean(bool $consent)
    {
        if ($consent) {
            return self::fromString(self::YES);
        }

        return self::fromString(self::NO);
    }

    public function toBoolean(): bool
    {
        return $this->consent === self::YES;
    }

    public function __toString(): string
    {
        return $this->consent;
    }

    public function toString(): string
    {
        return $this->consent;
    }
}
