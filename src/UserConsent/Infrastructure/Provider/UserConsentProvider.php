<?php

declare(strict_types=1);

namespace Glance\PhotoService\UserConsent\Infrastructure\Provider;

use Glance\PhotoService\Shared\Domain\Consent;
use RuntimeException;
use Glance\PhotoService\Shared\Domain\PersonId;
use Glance\PhotoService\UserConsent\Domain\AgentId;
use Glance\PhotoService\UserConsent\Application\GetUserConsentDetails\UserConsent;
use Glance\PhotoService\UserConsent\Application\GetUserConsentDetails\GetUserConsentDetailsRepositoryInterface;
use Glance\PhotoService\UserConsent\Application\GetUserConsentHistoryDetails\GetUserConsentHistoryDetailsRepositoryInterface;
use Glance\PhotoService\UserConsent\Application\UpdateUserConsent\UpdateUserConsentCommand;
use Glance\PhotoService\UserConsent\Application\UpdateUserConsent\UpdateUserConsentHandler;

class UserConsentProvider
{
    private $userConsentDetailsViewRepository;
    private $userConsentHistoryDetailsViewRepository;
    private $updateUserConsentHandler;

    public function __construct(
        GetUserConsentDetailsRepositoryInterface $userConsentDetailsViewRepository,
        GetUserConsentHistoryDetailsRepositoryInterface $userConsentHistoryDetailsViewRepository,
        UpdateUserConsentHandler $updateUserConsentHandler
    ) {
        $this->userConsentDetailsViewRepository = $userConsentDetailsViewRepository;
        $this->userConsentHistoryDetailsViewRepository = $userConsentHistoryDetailsViewRepository;
        $this->updateUserConsentHandler = $updateUserConsentHandler;
    }

    public function findUserConsentDetailsByPersonId(
        int $personId,
        string $applicationId
    ): ?UserConsent {
        return $this->userConsentDetailsViewRepository->findUserConsentDetailsByPersonId(
            PersonId::fromInteger($personId),
            $applicationId
        );
    }

    public function findUserConsentDetailsByApplicationId(
        string $applicationId
    ): array {
        return $this->userConsentDetailsViewRepository->findUserConsentDetailsByApplicationId(
            $applicationId
        );
    }

    public function findUserConsentHistoryDetailsByPersonId(
        int $personId,
        string $applicationId
    ): ?array {
        return $this->userConsentHistoryDetailsViewRepository->findUserConsentHistoryDetailsByPersonId(
            PersonId::fromInteger($personId),
            $applicationId
        );
    }

    public function findUserConsentHistoryDetailsByApplicationId(
        string $applicationId
    ): ?array {
        return $this->userConsentHistoryDetailsViewRepository->findUserConsentHistoryDetailsByApplicationId(
            $applicationId
        );
    }

    public function updateUserConsent(
        int $personId,
        bool $consent,
        string $applicationId,
        int $agentId
    ): ?UserConsent {
        $command = UpdateUserConsentCommand::fromInternalRequest(
            PersonId::fromInteger($personId),
            Consent::fromBoolean($consent),
            AgentId::fromInteger($agentId),
            $applicationId
        );

        try {
            $this->updateUserConsentHandler->handle($command);
        } catch (\Throwable $th) {
            throw new RuntimeException("Caught: " . $th);
        }

        return $this->userConsentDetailsViewRepository->findUserConsentDetailsByPersonId(
            PersonId::fromInteger($personId),
            $applicationId
        );
    }
}
