<?php

namespace Glance\PhotoService\UserConsent\Infrastructure\Provider;

use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Driver\OCI8\OCI8Connection;
use DI\ContainerBuilder;
use Glance\PhotoService\UserConsent\Domain\UserConsentRepositoryInterface;
use Glance\PhotoService\UserConsent\Infrastructure\Persistence\SqlConsentRepository;
use Glance\PhotoService\UserConsent\Infrastructure\Provider\UserConsentProvider;
use Glance\PhotoService\UserConsent\Application\UpdateUserConsent\UpdateUserConsentHandler;
use Glance\PhotoService\UserConsent\Application\GetUserConsentDetails\GetUserConsentDetailsRepositoryInterface;
use Glance\PhotoService\UserConsent\Application\GetUserConsentHistoryDetails\GetUserConsentHistoryDetailsRepositoryInterface;

final class UserConsentProviderFactory
{
    public static function getUserConsentInstance(
        string $dbName,
        string $dbPasswd,
        string $dbDns
    ): UserConsentProvider {
        $definitions = [
            Connection::class => function () use ($dbName, $dbPasswd, $dbDns) {
                $connection = new OCI8Connection(
                    $dbName,
                    $dbPasswd,
                    $dbDns
                );
                return $connection;
            },
            UserConsentRepositoryInterface::class => function (Connection $conn) {
                return new SqlConsentRepository($conn);
            },
            GetUserConsentDetailsRepositoryInterface::class => function (Connection $conn) {
                return new SqlConsentRepository($conn);
            },
            GetUserConsentHistoryDetailsRepositoryInterface::class => function (Connection $conn) {
                return new SqlConsentRepository($conn);
            }
        ];

        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions($definitions);
        $container = $containerBuilder->build();
        return new UserConsentProvider(
            $container->get(GetUserConsentDetailsRepositoryInterface::class),
            $container->get(GetUserConsentHistoryDetailsRepositoryInterface::class),
            $container->get(UpdateUserConsentHandler::class)
        );
    }
}
