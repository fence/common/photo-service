<?php

declare(strict_types=1);

namespace Glance\PhotoService\UserConsent\Infrastructure\Persistence;

use Doctrine\Dbal\Driver\Connection;
use Glance\PhotoService\UserConsent\Domain\ModifiedOn;
use Glance\PhotoService\UserConsent\Domain\UserConsent;
use Glance\PhotoService\UserConsent\Domain\UserConsentRepositoryInterface;
use Glance\PhotoService\Shared\Domain\PersonId;
use Glance\PhotoService\UserConsent\Application\GetUserConsentDetails\GetUserConsentDetailsRepositoryInterface;
use Glance\PhotoService\UserConsent\Application\GetUserConsentDetails\UserConsent as UserConsentDetails;
use Glance\PhotoService\UserConsent\Application\GetUserConsentHistoryDetails\GetUserConsentHistoryDetailsRepositoryInterface;
use Glance\PhotoService\UserConsent\Application\GetUserConsentHistoryDetails\UserConsentHistory as UserConsentHistoryDetails;

class SqlConsentRepository implements
    UserConsentRepositoryInterface,
    GetUserConsentDetailsRepositoryInterface,
    GetUserConsentHistoryDetailsRepositoryInterface
{
    private $connection;

    public function __construct(
        Connection $connection
    ) {
        $this->connection = $connection;
    }

    public function findConsentByPersonId(PersonId $personId, string $applicationId): ?UserConsent
    {
        $query = "SELECT
                AP.PERSON_ID,
                AP.CONSENT,
                AP.APPLICATION_ID,
                AP.AGENT_PERSON_ID
            FROM  ALLOW_PHOTO AP
            WHERE
                AP.PERSON_ID = :personId
                AND AP.APPLICATION_ID = :applicationId
        ";

        $statement = $this->connection->prepare($query);
        $payload = [
            "personId" => $personId->toInteger(),
            "applicationId" => $applicationId
        ];
        $statement->execute($payload);
        $rows = $statement->fetchAll();

        if (empty($rows)) {
            return null;
        }

        return !empty($rows) ? UserConsent::fromPersistence($rows[0]) : null;
    }

    public function findUserConsentDetailsByPersonId(PersonId $personId, string $applicationId): ?UserConsentDetails
    {
        $query = "SELECT
                AP.PERSON_ID,
                AP.CONSENT,
                AP.APPLICATION_ID,
                AP.AGENT_PERSON_ID AS AGENT_ID,
                AP.MODIFIED_ON AS MODIFIED_ON
            FROM ALLOW_PHOTO AP
            WHERE
                AP.PERSON_ID = :personId
                AND AP.APPLICATION_ID = :applicationId
        ";
        $statement = $this->connection->prepare($query);
        $payload = [
            "personId" => $personId->toInteger(),
            "applicationId" => $applicationId
        ];
        $statement->execute($payload);
        $rows = $statement->fetchAll();

        if (empty($rows)) {
            return null;
        }

        return UserConsentDetails::fromPersistence($rows[0]);
    }


    public function updateConsent(UserConsent $userConsent): void
    {
        $query = "UPDATE ALLOW_PHOTO
            SET
                AGENT_PERSON_ID = :varAgentId,
                CONSENT = :varConsent,
                MODIFIED_ON = TO_DATE(:varModifiedOn, 'YYYY-MM-DD HH24:MI:SS')
            WHERE
                PERSON_ID = :varPersonId
                AND APPLICATION_ID = :varApplicationId"
        ;

        $payload = [
            "varApplicationId" => $userConsent->applicationId(),
            "varPersonId" => $userConsent->personId()->toInteger(),
            "varConsent" => $userConsent->consent()->toString(),
            "varModifiedOn" => ModifiedOn::now()->toString(),
            "varAgentId" => $userConsent->agentId()->toInteger()
        ];

        $statement = $this->connection->prepare($query);
        $statement->execute($payload);
        $this->connection->commit();
    }

    public function insertConsent(UserConsent $userConsent): void
    {
        $query = "INSERT INTO ALLOW_PHOTO (
                APPLICATION_ID,
                PERSON_ID,
                AGENT_PERSON_ID,
                CONSENT,
                MODIFIED_ON
            ) VALUES (
                :varApplicationId,
                :varPersonId,
                :varAgentId,
                :varConsent,
                TO_DATE(:varModifiedOn, 'YYYY-MM-DD HH24:MI:SS')
            )"
        ;

        $payload = [
            "varApplicationId" => $userConsent->applicationId(),
            "varPersonId" => $userConsent->personId()->toInteger(),
            "varConsent" => $userConsent->consent()->toString(),
            "varModifiedOn" => ModifiedOn::now()->toString(),
            "varAgentId" => $userConsent->agentId()->toInteger()
        ];

        $statement = $this->connection->prepare($query);
        $statement->execute($payload);
        $this->connection->commit();
    }

    /** @return UserConsent[] */
    public function findUserConsentDetailsByApplicationId(string $applicationId): array
    {
        $query = "SELECT
                AP.PERSON_ID,
                AP.CONSENT,
                AP.APPLICATION_ID,
                AP.AGENT_PERSON_ID AS AGENT_ID,
                AP.MODIFIED_ON AS MODIFIED_ON
            FROM ALLOW_PHOTO AP
            WHERE
                AP.APPLICATION_ID = :applicationId
        ";
        $statement = $this->connection->prepare($query);
        $payload = [
            "applicationId" => $applicationId
        ];
        $statement->execute($payload);

        return array_map(function (array $row) {
            return UserConsentDetails::fromPersistence($row);
        }, $statement->fetchAll());
    }

    /** @return UserConsentHistory[] */
    public function findUserConsentHistoryDetailsByPersonId(PersonId $personId, string $applicationId): ?array
    {
        $query = "SELECT
                AP.ID,
                AP.APPLICATION_ID,
                AP.PERSON_ID,
                AP.CONSENT,
                TO_CHAR(AP.MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS.FF6') AS MODIFIED_ON,
                AP.AGENT_PERSON_ID,
                AP.ACTION,
                AP.DIFF_COLUMN,
                AP.OLD_VALUE,
                AP.NEW_VALUE
            FROM GLANCE_PHOTO_HIST.ALLOW_PHOTO AP
            WHERE 
                AP.PERSON_ID = :personId
                AND AP.APPLICATION_ID = :applicationId
        ";

        $statement = $this->connection->prepare($query);
        $payload = [
            "personId" => $personId->toInteger(),
            "applicationId" => $applicationId
        ];
        $statement->execute($payload);
        $rows = $statement->fetchAll();

        if (empty($rows)) {
            return null;
        }

        return array_map(function (array $row) {
            return UserConsentHistoryDetails::fromPersistence($row);
        }, $rows);
    }

    /** @return UserConsentHistory[] */
    public function findUserConsentHistoryDetailsByApplicationId(string $applicationId): ?array
    {
        $query = "SELECT
                AP.ID,
                AP.APPLICATION_ID,
                AP.PERSON_ID,
                AP.CONSENT,
                TO_CHAR(AP.MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS.FF6') AS MODIFIED_ON,
                AP.AGENT_PERSON_ID,
                AP.ACTION,
                AP.DIFF_COLUMN,
                AP.OLD_VALUE,
                AP.NEW_VALUE
            FROM GLANCE_PHOTO_HIST.ALLOW_PHOTO AP
            WHERE AP.APPLICATION_ID = :applicationId
        ";

        $statement = $this->connection->prepare($query);
        $payload = [
            "applicationId" => $applicationId
        ];
        $statement->execute($payload);
        $rows = $statement->fetchAll();

        if (empty($rows)) {
            return null;
        }

        return array_map(function (array $row) {
            return UserConsentHistoryDetails::fromPersistence($row);
        }, $rows);
    }
}
