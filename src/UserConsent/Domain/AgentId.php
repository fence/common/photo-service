<?php

declare(strict_types=1);

namespace Glance\PhotoService\UserConsent\Domain;

use Glance\PhotoService\Shared\Domain\IntegerId;

final class AgentId extends IntegerId
{
}
