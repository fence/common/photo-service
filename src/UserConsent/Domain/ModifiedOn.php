<?php

declare(strict_types=1);

namespace Glance\PhotoService\UserConsent\Domain;

use DateTimeImmutable;

final class ModifiedOn
{
    private const DEFAULT_DATE_FORMAT = "Y-m-d H:i:s";

    /**
     * Modification date
     *
     * @var DateTimeImmutable
     */
    protected $date;

    public function __construct($date)
    {
        $this->date = new DateTimeImmutable($date);
    }

    public static function fromString(string $date = "now"): self
    {
        return new self($date);
    }

    public static function now(): self
    {
        return new self("now");
    }

    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    public function toString($format = null): string
    {
        if (is_null($format)) {
            return $this->date->format(self::DEFAULT_DATE_FORMAT);
        }

        return $this->date->format($format);
    }

    public function __toString()
    {
        return $this->toString();
    }
}
