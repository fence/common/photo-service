<?php

namespace Glance\PhotoService\UserConsent\Domain;

use Glance\PhotoService\Shared\Domain\PersonId;

interface UserConsentRepositoryInterface
{
    public function findConsentByPersonId(PersonId $personId, string $applicationId): ?UserConsent;
    public function updateConsent(UserConsent $userConsent): void;
    public function insertConsent(UserConsent $userConsent): void;
}
