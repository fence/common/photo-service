<?php

declare(strict_types=1);

namespace Glance\PhotoService\UserConsent\Domain;

use Glance\PhotoService\Shared\Domain\Consent;
use Glance\PhotoService\Shared\Domain\PersonId;

/**
 *
 * Domain model for the User Consent.
 *
 * @author Gabriel Aleksandravicius <gabriel.aleks@cern.ch>
 */
class UserConsent
{
    private $personId;
    private $consent;
    private $applicationId;
    private $agentId;

    public function __construct(
        PersonId $personId,
        Consent $consent,
        string $applicationId,
        ?AgentId $agentId
    ) {
        $this->personId = $personId;
        $this->consent = $consent;
        $this->applicationId = $applicationId;
        $this->agentId = $agentId;
    }

    public static function create(
        PersonId $personId,
        Consent $consent,
        string $applicationId,
        ?AgentId $agentId
    ): self {
        $newUserConsent = new self(
            $personId,
            $consent,
            $applicationId,
            $agentId
        );
        return $newUserConsent;
    }

    public static function fromPersistence(
        array $row
    ): self {
        return new self(
            PersonId::fromString($row["PERSON_ID"]),
            Consent::fromString($row["CONSENT"]),
            $row["APPLICATION_ID"],
            isset($row["AGENT_PERSON_ID"]) ? AgentId::fromString($row["AGENT_PERSON_ID"]) : null
        );
    }

    public function personId(): PersonId
    {
        return $this->personId;
    }

    public function consent(): Consent
    {
        return $this->consent;
    }

    public function applicationId(): string
    {
        return $this->applicationId;
    }

    public function agentId(): AgentId
    {
        return $this->agentId;
    }

    public function updateConsent(
        Consent $consent,
        AgentId $agentId
    ): void {
        $this->consent = $consent;
        $this->agentId = $agentId;
    }
}
