<?php

declare(strict_types=1);

namespace Glance\PhotoService\UserConsent\Application\GetUserConsentHistoryDetails;

use Glance\PhotoService\Shared\Domain\IntegerId;
use Glance\PhotoService\Shared\Domain\PersonId;
use Glance\PhotoService\Shared\Domain\Consent;
use Glance\PhotoService\UserConsent\Domain\ModifiedOn;
use Glance\PhotoService\UserConsent\Domain\AgentId;

final class UserConsentHistory
{
    private $id;
    private $applicationId;
    private $personId;
    private $consent;
    private $modifiedOn;
    private $agentId;
    private $action;
    private $diffColumn;
    private $oldValue;
    private $newValue;

    private function __construct(
        IntegerId $id,
        PersonId $personId,
        string $applicationId,
        Consent $consent,
        ModifiedOn $modifiedOn,
        AgentId $agentId,
        string $action,
        ?string $diffColumn,
        ?string $oldValue,
        ?string $newValue
    ) {
        $this->id = $id;
        $this->personId = $personId;
        $this->applicationId = $applicationId;
        $this->consent = $consent;
        $this->modifiedOn = $modifiedOn;
        $this->agentId = $agentId;
        $this->action = $action;
        $this->diffColumn = $diffColumn;
        $this->oldValue = $oldValue;
        $this->newValue = $newValue;
    }

    public static function fromPersistence(array $row): self
    {
        return new self(
            IntegerId::fromString($row["ID"]),
            PersonId::fromString($row["PERSON_ID"]),
            $row["APPLICATION_ID"],
            Consent::fromString($row["CONSENT"]),
            ModifiedOn::fromString($row["MODIFIED_ON"]),
            AgentId::fromString($row["AGENT_PERSON_ID"]),
            $row["ACTION"],
            isset($row["DIFF_COLUMN"]) ? $row["DIFF_COLUMN"] : null,
            isset($row["OLD_VALUE"]) ? $row["OLD_VALUE"] : null,
            isset($row["NEW_VALUE"]) ? $row["NEW_VALUE"] : null,
        );
    }

    public function id(): IntegerId
    {
        return $this->id;
    }

    public function personId(): PersonId
    {
        return $this->personId;
    }

    public function applicationId(): string
    {
        return $this->applicationId;
    }

    public function consent(): Consent
    {
        return $this->consent;
    }

    public function modifiedOn(): ModifiedOn
    {
        return $this->modifiedOn;
    }

    public function agentId(): AgentId
    {
        return $this->agentId;
    }

    public function action(): string
    {
        return $this->action;
    }

    public function diffColumn(): ?string
    {
        return $this->diffColumn;
    }

    public function oldValue(): ?string
    {
        return $this->oldValue;
    }

    public function newValue(): ?string
    {
        return $this->newValue;
    }
}
