<?php

declare(strict_types=1);

namespace Glance\PhotoService\UserConsent\Application\GetUserConsentHistoryDetails;

use Glance\PhotoService\Shared\Domain\PersonId;

interface GetUserConsentHistoryDetailsRepositoryInterface
{
    public function findUserConsentHistoryDetailsByPersonId(PersonId $personId, string $applicationId): ?array;
    public function findUserConsentHistoryDetailsByApplicationId(string $applicationId): ?array;
}
