<?php

declare(strict_types=1);

namespace Glance\PhotoService\UserConsent\Application\UpdateUserConsent;

use Glance\PhotoService\Shared\Domain\Consent;
use Glance\PhotoService\UserConsent\Domain\AgentId;
use Glance\PhotoService\Shared\Domain\PersonId;

class UpdateUserConsentCommand
{
    private $personId;
    private $consent;
    private $agentId;
    private $applicationId;

    public static function fromInternalRequest(
        PersonId $personId,
        Consent $consent,
        AgentId $agentId,
        string $applicationId
    ): self {
        $command = new self();

        $command->personId = $personId;
        $command->consent = $consent;
        $command->agentId = $agentId;
        $command->applicationId = $applicationId;

        return $command;
    }

    public function personId(): PersonId
    {
        return $this->personId;
    }

    public function consent(): Consent
    {
        return $this->consent;
    }

    public function agentId(): AgentId
    {
        return $this->agentId;
    }

    public function applicationId(): string
    {
        return $this->applicationId;
    }
}
