<?php

declare(strict_types=1);

namespace Glance\PhotoService\UserConsent\Application\UpdateUserConsent;

use Glance\PhotoService\UserConsent\Domain\UserConsent;
use Glance\PhotoService\UserConsent\Domain\UserConsentRepositoryInterface;

class UpdateUserConsentHandler
{
    private $userConsentRepository;

    public function __construct(
        UserConsentRepositoryInterface $userConsentRepository
    ) {
        $this->userConsentRepository = $userConsentRepository;
    }

    public function handle(UpdateUserConsentCommand $command): UserConsent
    {
        $userConsent = $this->userConsentRepository->findConsentByPersonId(
            $command->personId(),
            $command->applicationId()
        );

        /** Register new consent entry when member answers consent request for the first time */
        if (!$userConsent) {
            $userConsent = UserConsent::create(
                $command->personId(),
                $command->consent(),
                $command->applicationId(),
                $command->agentId()
            );
            $this->userConsentRepository->insertConsent($userConsent);
            return $userConsent;
        }

        /** Updates user consent in case it's already been provided in the past */
        $userConsent->updateConsent($command->consent(), $command->agentId());
        $this->userConsentRepository->updateConsent($userConsent);
        return $userConsent;
    }
}
