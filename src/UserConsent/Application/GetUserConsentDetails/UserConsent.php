<?php

declare(strict_types=1);

namespace Glance\PhotoService\UserConsent\Application\GetUserConsentDetails;

use Glance\PhotoService\Shared\Domain\Consent;
use Glance\PhotoService\UserConsent\Domain\ModifiedOn;
use Glance\PhotoService\UserConsent\Domain\AgentId;
use Glance\PhotoService\Shared\Domain\PersonId;
use JsonSerializable;

final class UserConsent implements JsonSerializable
{
    private $personId;
    private $consent;
    private $applicationId;
    private $agentId;
    private $modifiedOn;

    private function __construct(
        PersonId $personId,
        Consent $consent,
        string $applicationId,
        ?AgentId $agentId,
        ?ModifiedOn $modifiedOn
    ) {
        $this->personId = $personId;
        $this->consent = $consent;
        $this->applicationId = $applicationId;
        $this->agentId = $agentId;
        $this->modifiedOn = $modifiedOn;
    }

    public static function fromPersistence(array $row): self
    {
        return new self(
            PersonId::fromString($row["PERSON_ID"]),
            Consent::fromString($row["CONSENT"]),
            $row["APPLICATION_ID"],
            isset($row["AGENT_ID"]) ? AgentId::fromString($row["AGENT_ID"]) : null,
            isset($row["MODIFIED_ON"]) ? ModifiedOn::fromString($row["MODIFIED_ON"]) : null,
        );
    }

    public function personId(): PersonId
    {
        return $this->personId;
    }

    public function hasConsent(): bool
    {
        return $this->consent->toBoolean();
    }

    public function consent(): Consent
    {
        return $this->consent;
    }

    public function applicationId(): string
    {
        return $this->applicationId;
    }

    public function agentId(): ?AgentId
    {
        return $this->agentId;
    }

    public function modifiedOn(): ?ModifiedOn
    {
        return $this->modifiedOn;
    }

    public function jsonSerialize(): array
    {
        return [
            "personId" => $this->personId->toInteger(),
            "consent" => $this->consent->toString(),
            "applicationId" => $this->applicationId,
            "agentId" => $this->agentId ? $this->agentId->toInteger() : null,
            "modifiedOn" => $this->modifiedOn ? $this->modifiedOn->toString() : null,
        ];
    }
}
