<?php

declare(strict_types=1);

namespace Glance\PhotoService\UserConsent\Application\GetUserConsentDetails;

use Glance\PhotoService\Shared\Domain\PersonId;

interface GetUserConsentDetailsRepositoryInterface
{
    public function findUserConsentDetailsByPersonId(PersonId $personId, string $applicationId): ?UserConsent;

    public function findUserConsentDetailsByApplicationId(string $applicationId): array;
}
