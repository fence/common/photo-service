<?php

namespace Glance\PhotoService\Authorization\Domain;

use Glance\PhotoService\Authorization\Exception\UserHasNotGivenConsent;

class PhotoAuthorization
{
    public function applicationCanFetchPhoto(
        $userHasGivenConsentToApplication
    ): bool {
        if (!$userHasGivenConsentToApplication) {
            throw new UserHasNotGivenConsent("User has not given consent");
        }
        return true;
    }
}
