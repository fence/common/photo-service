<?php

namespace Glance\PhotoService\Authorization\Domain;

use Glance\PhotoService\Shared\Domain\PersonId;

interface AuthorizationRepositoryInterface
{
    public function personHasGivenConsentToApplication(
        PersonId $personId,
        string $applicationId
    ): bool;
}
