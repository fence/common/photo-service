<?php

namespace Glance\PhotoService\Authorization\Exception;

use Exception;

class UserHasNotGivenConsent extends Exception
{
}
