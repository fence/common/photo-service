<?php

namespace Glance\PhotoService\Authorization\Application\ApplicationCanFetchPhoto;

use Glance\PhotoService\Authorization\Domain\AuthorizationRepositoryInterface;
use Glance\PhotoService\Authorization\Domain\PhotoAuthorization;
use Glance\PhotoService\Shared\Domain\PersonId;

class ApplicationCanFetchPhoto
{
    private $authorizationRepository;
    private $photoAuthorization;

    public function __construct(
        AuthorizationRepositoryInterface $authorizationRepository,
        PhotoAuthorization $photoAuthorization
    ) {
        $this->authorizationRepository = $authorizationRepository;
        $this->photoAuthorization = $photoAuthorization;
    }

    public function clear(
        PersonId $personId,
        string $applicationId
    ): void {
        $this->photoAuthorization->applicationCanFetchPhoto(
            $this->authorizationRepository->personHasGivenConsentToApplication(
                $personId,
                $applicationId
            )
        );
    }
}
