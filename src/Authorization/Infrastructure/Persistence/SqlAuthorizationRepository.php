<?php

declare(strict_types=1);

namespace Glance\PhotoService\Authorization\Infrastructure\Persistence;

use Glance\PhotoService\Authorization\Domain\AuthorizationRepositoryInterface;
use Glance\PhotoService\Shared\Domain\PersonId;
use Glance\PhotoService\UserConsent\Infrastructure\Provider\UserConsentProvider;

final class SqlAuthorizationRepository implements AuthorizationRepositoryInterface
{
    private $userConsentProvider;

    public function __construct(UserConsentProvider $userConsentProvider)
    {
        $this->userConsentProvider = $userConsentProvider;
    }

    /**
     * Returns false when user rejects consent or has not replied yet
     * true when user gives consent.
     */
    public function personHasGivenConsentToApplication(PersonId $personId, string $applicationId): bool
    {
        $consent = $this->userConsentProvider->findUserConsentDetailsByPersonId(
            $personId->toInteger(),
            $applicationId
        );
        return $consent ? $consent->hasConsent() : false;
    }
}
