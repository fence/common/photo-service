<?php

namespace Glance\PhotoService\Photo\Domain;

class Photo
{
    private $binaryContent;

    private function __construct(
        string $binaryContent
    ) {
        $this->binaryContent = $binaryContent;
    }

    public static function fromBinary(string $binaryContent): self
    {
        return new self(
            $binaryContent
        );
    }

    public function binaryContent(): string
    {
        return $this->binaryContent;
    }

    public function base64Encoded(): ?string
    {
        return base64_encode($this->binaryContent);
    }

    public function extension(): ?string
    {
        $f = finfo_open();
        $mimeType = finfo_buffer($f, $this->binaryContent, FILEINFO_MIME_TYPE);
        finfo_close($f);
        $map = [
            "image/png" => "png",
            "image/jpeg" => "jpg",
            "image/gif" => "gif",
            "application/pdf" => "pdf"
        ];

        return array_key_exists($mimeType, $map) ? $map[$mimeType] : null;
    }
}
