<?php

namespace Glance\PhotoService\Photo\Application\GetPhoto;

use Glance\PhotoService\Photo\Domain\Photo;
use Glance\PhotoService\Shared\Domain\PersonId;

interface PhotoViewRepositoryInterface
{
    public function getPhoto(PersonId $personId): Photo;
}
