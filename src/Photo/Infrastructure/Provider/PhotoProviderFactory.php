<?php

namespace Glance\PhotoService\Photo\Infrastructure\Provider;

use Doctrine\DBAL\Driver\Connection;
use DI\ContainerBuilder;
use Doctrine\DBAL\Driver\OCI8\OCI8Connection;
use Glance\PhotoService\Authorization\Application\ApplicationCanFetchPhoto\ApplicationCanFetchPhoto;
use Glance\PhotoService\Authorization\Domain\AuthorizationRepositoryInterface;
use Glance\PhotoService\Authorization\Domain\PhotoAuthorization;
use Glance\PhotoService\Authorization\Infrastructure\Persistence\SqlAuthorizationRepository;
use Glance\PhotoService\Photo\Application\GetPhoto\PhotoViewRepositoryInterface;
use Glance\PhotoService\Photo\Infrastructure\Persistence\RestPhotoViewRepository;
use Glance\PhotoService\UserConsent\Infrastructure\Provider\UserConsentProviderFactory;
use Glance\PhotoService\Shared\Domain\Url;
use Glance\PhotoService\UserConsent\Infrastructure\Provider\UserConsentProvider;
use GuzzleHttp\Client;

abstract class PhotoProviderFactory
{
    public static function getPhotoProvider(
        string $username,
        string $password,
        string $experimentEndpoint,
        string $dbName,
        string $dbPasswd,
        string $dbDns
    ): PhotoProvider {
        $definitions = [
            Connection::class => function () use ($dbName, $dbPasswd, $dbDns) {
                $connection = new OCI8Connection(
                    $dbName,
                    $dbPasswd,
                    $dbDns
                );
                return $connection;
            },
            PhotoViewRepositoryInterface::class => function (Client $client) use ($username, $password, $experimentEndpoint) {
                return new RestPhotoViewRepository(
                    $client,
                    $username,
                    $password,
                    Url::fromString($experimentEndpoint)
                );
            },
            UserConsentProvider::class => function () use ($dbName, $dbPasswd, $dbDns) {
                return UserConsentProviderFactory::getUserConsentInstance(
                    $dbName,
                    $dbPasswd,
                    $dbDns
                );
            },
            AuthorizationRepositoryInterface::class => function (UserConsentProvider $provider) {
                return new SqlAuthorizationRepository($provider);
            },
            ApplicationCanFetchPhoto::class => function (AuthorizationRepositoryInterface $repository, PhotoAuthorization $authorization) {
                return new ApplicationCanFetchPhoto($repository, $authorization);
            }
        ];
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions($definitions);
        $container = $containerBuilder->build();
        return new PhotoProvider(
            $container->get(PhotoViewRepositoryInterface::class),
            $container->get(ApplicationCanFetchPhoto::class)
        );
    }
}
