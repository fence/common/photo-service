<?php

namespace Glance\PhotoService\Photo\Infrastructure\Provider;

use Glance\PhotoService\Authorization\Application\ApplicationCanFetchPhoto\ApplicationCanFetchPhoto;
use Glance\PhotoService\Photo\Application\GetPhoto\PhotoViewRepositoryInterface;
use Glance\PhotoService\Photo\Domain\Photo;
use Glance\PhotoService\Shared\Domain\PersonId;

class PhotoProvider
{
    private $photoViewRepository;
    private $applicationCanFetchPhoto;

    public function __construct(
        PhotoViewRepositoryInterface $photoViewRepository,
        ApplicationCanFetchPhoto $applicationCanFetchPhoto
    ) {
        $this->photoViewRepository = $photoViewRepository;
        $this->applicationCanFetchPhoto = $applicationCanFetchPhoto;
    }

    public function getPhoto(int $personId, string $applicationId): Photo
    {
        $personId = PersonId::fromInteger($personId);
        $this->applicationCanFetchPhoto->clear($personId, $applicationId);
        return $this->photoViewRepository->getPhoto($personId);
    }
}
