<?php

declare(strict_types=1);

namespace Glance\PhotoService\Photo\Infrastructure\Exception;

class UnableToFetchFromAdamsException extends \Exception
{
    public static function withCustomReason(string $reason): self
    {
        return new self("Unable to reach the Adams API. Reason: {$reason}");
    }
}
