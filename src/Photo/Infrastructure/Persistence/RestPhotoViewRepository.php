<?php

declare(strict_types=1);

namespace Glance\PhotoService\Photo\Infrastructure\Persistence;

use Glance\PhotoService\Photo\Application\GetPhoto\PhotoViewRepositoryInterface;
use Glance\PhotoService\Photo\Domain\Photo;
use Glance\PhotoService\Photo\Infrastructure\Exception\UnableToFetchFromAdamsException;
use Glance\PhotoService\Shared\Domain\PersonId;
use Glance\PhotoService\Shared\Domain\Url;
use GuzzleHttp\Client;
use Throwable;

class RestPhotoViewRepository implements PhotoViewRepositoryInterface
{
    private $client;
    private $username;
    private $password;
    private $experimentEndpoint;

    public function __construct(
        Client $apiClient,
        string $username,
        string $password,
        Url $experimentEndpoint
    ) {
        $this->client = $apiClient;
        $this->username = $username;
        $this->password = $password;
        $this->experimentEndpoint = $experimentEndpoint;
    }

    private function authorizationHeader(): array
    {
        $credentials = base64_encode("$this->username:$this->password");
        return ["Basic $credentials"];
    }

    private function endpoint(PersonId $personId): string
    {
        return $this->experimentEndpoint->toString() . "/" . $personId->toString();
    }

    private function getRequestHeaders(): array
    {
        return [
            "Content-Type" => "application/json",
            "Authorization" => $this->authorizationHeader()
        ];
    }

    public function getPhoto(
        PersonId $personId
    ): Photo {
        $options = [
            "headers" => $this->getRequestHeaders(),
            "timeout" => 2,  // Set the timeout to 2 seconds
            "connect_timeout" => 1, // Set the connect timeout to 1 second
        ];
        try {
            $response = $this->client->get(
                $this->endpoint($personId),
                $options
            );
            return Photo::fromBinary($response->getBody()->getContents());
        } catch (Throwable $th) {
            throw UnableToFetchFromAdamsException::withCustomReason($th->getMessage());
        }
    }
}
