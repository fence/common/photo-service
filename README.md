# Photo Service
Repository for the common service used by the GLANCE team to manage photo consent.

## Usage
### Install
Install using composer:
```
composer-require glance-project/photo-service
```

### Usecases
#### Get User Consent

Find members who have registered their consent in certain application:
```php
use Glance\PhotoService\UserConsent\Infrastructure\Provider\UserConsentProviderFactory;
$provider = UserConsentProviderFactory::getUserConsentInstance(
    "<photo-db-username>",
    "<photo-db-password>",
    "<photo-db-dns>"
);
$details = $provider->findUserConsentDetailsByApplicationId("<your-application-id>");
```

Find consent of given member in your application:
```php
use Glance\PhotoService\UserConsent\Infrastructure\Provider\UserConsentProviderFactory;
$provider = UserConsentProviderFactory::getUserConsentInstance(
    "<photo-db-username>",
    "<photo-db-password>",
    "<photo-db-dns>"
);
$details = $provider->findUserConsentDetailsByPersonId(<member-person-id>,"<your-application-id>");
```

#### Get User Consent History

To get the consent history based on the person id of the member:
```php
use Glance\PhotoService\UserConsent\Infrastructure\Provider\UserConsentProviderFactory;
$provider = UserConsentProviderFactory::getUserConsentInstance(
    "<photo-db-username>",
    "<photo-db-password>",
    "<photo-db-dns>"
);
$details = $provider->findUserConsentHistoryDetailsByPersonId(<member-person-id>, "<your-application-id>");
```

To get the consent history based on the application id only:
```php
use Glance\PhotoService\UserConsent\Infrastructure\Provider\UserConsentProviderFactory;
$provider = UserConsentProviderFactory::getUserConsentInstance(
    "<photo-db-username>",
    "<photo-db-password>",
    "<photo-db-dns>"
);
$details = $provider->findUserConsentHistoryDetailsByApplicationId("<your-application-id>");
```

#### Update Consent

```php
use Glance\PhotoService\UserConsent\Infrastructure\Provider\UserConsentProviderFactory;
$provider = UserConsentProviderFactory::getUserConsentInstance(
    "<photo-db-username>",
    "<photo-db-password>",
    "<photo-db-dns>"
);
$details = $provider->updateUserConsent(
            <member-person-id>,
            (bool) <new-consent>,
            "<your-application-id>",
            <agent-person-id>
        );
```

#### Get Photo
A `UnableToFetchFromAdamsException` is thrown if the ADAMS api is not answering correctly.
A `UserHasNotGivenConsent` is thrown if the user has not given consent.
```php
use Glance\PhotoService\Photo\Infrastructure\Provider\PhotoProviderFactory;
$photoProvider = PhotoProviderFactory::getPhotoProvider(
            "<service-account-username>",
            "<service-account-password>",
            "<experiment-api-endpoint>",
            "<photo-db-username>",
            "<photo-db-password>",
            "<photo-db-dns>"
        );

$photoProvider->getPhoto(
            <person-id>,
            "<application-id>"
        );
```

## Test environment setup
We rely on `.env` files to handle secret values.
Therefore, create a new `.env.local`, if it doesn't yet exist, and copy everything from `.env` to the local one. The database credentials must be from the photo DB.

```
# .env.local
DB_USERNAME=<username>
DB_DNS=<dns>
DB_PASSWORD=<password>
```
### Starting the container
In order to start developing, after you've cloned the repository you'll need to start the container for the service. To do that, run the following command:

```bash
docker-compose up -d
```

Once the container is up and running, execute `docker exec -it photo-service bash` so you are prompted to the container CLI.

### Installing php dependencies
In order to install the php dependencies, run `composer install` inside the container so composer will install exactly the specific versions set on the `composer.lock` file.